<?php
/**
 * Created by PhpStorm.
 * User: Marcus Dalgren
 * Date: 2015-02-07
 * Time: 18:38
 */

namespace MetaPic\Models;
use Eloquent;

class ImageView extends Eloquent {
	protected $table = "image_views";
	protected $fillable = array(
		"image_id",
		"ip",
		"url",
		"browser",
	);
}
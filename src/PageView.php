<?php

namespace MetaPic\Models;
use Eloquent;

class PageView extends Eloquent {
	protected $table = "page_views";
	protected $fillable = array(
		"user_id",
		"ip",
		"url",
		"browser",
	);
}
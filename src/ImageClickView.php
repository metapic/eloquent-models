<?php
/**
 * Created by PhpStorm.
 * User: Marcus Dalgren
 * Date: 2014-09-28
 * Time: 01:37
 */

namespace MetaPic\Models;


use Carbon\Carbon;
use Eloquent;

/**
 * An Eloquent Model: 'ImageClickView'
 *
 * @property integer                      $id
 * @property integer                      $image_id
 * @property integer                      $user_id
 * @property Carbon               		  $date
 * @property integer                      $clicks
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class ImageClickView extends Eloquent {
	protected $table = "image_click_views";
	protected $fillable = [
		"image_id",
		"user_id",
		"date",
		"clicks"
	];

	public function image() {
		return $this->belongsTo('MetaPic\Models\Image', 'image_id', 'id');
	}
} 
<?php
/**
 * Created by PhpStorm.
 * User: marcus
 * Date: 2014-03-04
 * Time: 12:11
 */

namespace MetaPic\Models;

use Cache;
use DB;
use Eloquent;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * An Eloquent Model: 'MetaPic\Models\Client'
 *
 * @property integer $id
 * @property string $client_id
 * @property string $name
 * @property string $secret_key
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\MetaPic\Models\User[] $users
 * @property-read \MetaPic\Models\ClientUserConfig $clientUserConfig
 */
class Client extends Eloquent {

	protected $table = 'clients';
	protected $hidden = array('secret_key','is_admin','has_user_clients');
	protected $guarded = array('client_id', 'secret_key');

	public static function getAuthData($clientId) {
		$client = self::getClient($clientId);
		return ["client_id" => $clientId, "secret_key" => $client->secret_key];
	}

	public function getIdAttribute($value) {
		return ($value == null) ? null : (int)$value;
	}

	/**
	 * @param string $clientId
	 * @throws ModelNotFoundException
	 * @return Client
	 */
	public static function getClient($clientId) {
		//return Cache::remember('client_' . $clientId, 0, function () use ($clientId) {
			$client = Client::where("client_id", "=", $clientId)->firstOrFail();
			return $client;
		//});
	}

	public function users() {
		return $this->hasMany('MetaPic\Models\User', 'client_id', 'id');
	}

	public function revenueTiers() {
		return $this->hasMany('MetaPic\Models\RevenueTier', 'client_id', 'id');
	}

	public function clientUserConfig() {
		return $this->hasOne('MetaPic\Models\ClientUserConfig', 'client_id', 'id');
	}

	public function storeGroup() {
		return $this->hasOne('MetaPic\Models\StoreGroup', 'id', 'store_group_id');
	}
} 
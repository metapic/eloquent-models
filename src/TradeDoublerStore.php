<?php
/**
 * Created by PhpStorm.
 * User: marcusdalgren
 * Date: 2017-08-14
 * Time: 11:37
 */

namespace MetaPic\Models;
use Eloquent;

/**
 * An Eloquent Model: 'MetaPic\Models\ShopelloStore'
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class TradeDoublerStore extends Eloquent {
	protected $primaryKey = 'feed_id';
	public $incrementing = false;
	protected $fillable = array(
		"feed_id",
		"name",
		"deleted",
		"active",
		"send_to_new_pf",
		"visible",
		"currency_iso_code",
		"language_iso_code",
		"secret",
		"advertiser_id",
		"unmapped_categories",
		"products",
		"program_id",
		"program_name",
		"revenue_cpc",
		"url",
		"locale"
	);
	protected $table = "stores_tradedoubler";
}
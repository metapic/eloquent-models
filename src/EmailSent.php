<?php
/**
 * Created by PhpStorm.
 * User: Erik Axelsson
 * Date: 2014-11-03
 * Time: 10:54
 */

namespace MetaPic\Models;

use Eloquent;
use Mandrill;
use Config;
/**
 * An Eloquent Model: 'MetaPic\Models\Tag'
 *
 */
class EmailSent extends Eloquent {
	protected $table = "email_sent";
	protected $fillable = array(
		"user_id",
        "template",
        "created_at"
	);


	public function user() {
		return $this->belongsTo('MetaPic\Models\User', 'user_id', 'id');
	}

    public static function sendEmail($templateName, User $user) {
        $mandrill = new Mandrill(Config::get('mandrill.api_key'));
        $template = $mandrill->templates->info($templateName);

        $message = array(
            'html' => $template['code'],
            'subject' => $template['subject'],
            'from_email' => 'support@metapic.se',
            'from_name' => 'Metapic',
            'to' => array(
                array(
                    'email' => $user->email,
                    'type' => 'to'
                )
            ),
            'headers' => array('Reply-To' => 'support@metapic.se'),
            'metadata' => array('website' => 'http://metapic.se'),
        );
        $async = true;


        $ip_pool = 'Main Pool';

        $result = $mandrill->messages->send($message, $async, $ip_pool);

        $email= new EmailSent();
        $email->user_id=$user->id;
        $email->template=$templateName;
        $email->save();



    }

}
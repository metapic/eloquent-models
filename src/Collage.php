<?php
/**
 * Created by PhpStorm.
 * User: bark
 * Date: 2014-12-18
 * Time: 15:51
 */
namespace MetaPic\Models;

use Eloquent;

class Collage extends Eloquent
{
    protected $table = "collage";

    protected $softDelete = true;
    protected $fillable = array(
        "img_url",
        "user_id",
        "product_list",
        "canavas_json",
        "json_format");

}


<?php
/**
 * Created by PhpStorm.
 * User: marcusdalgren
 * Date: 2016-03-16
 * Time: 16:57
 */

namespace MetaPic\Models;

use Eloquent;

class RevenueTier extends Eloquent  {
	protected $table = "user_revenue_tiers";

	public function users() {
		return $this->hasMany('MetaPic\Models\User', 'revenue_tier_id', 'id');
	}

	public function client() {
		return $this->belongsTo('MetaPic\Models\User', 'client_id', 'id');
	}

	public function scopeLowest($query) {

	}
}
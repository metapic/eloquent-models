<?php
/**
 * Created by PhpStorm.
 * User: bark
 * Date: 2014-12-18
 * Time: 15:51
 */
namespace MetaPic\Models;

use Eloquent;

class DeeplinkingUrls extends Eloquent
{
    protected $table = "deeplinking_url_v2";
    protected $softDelete = false;
    protected $fillable = ['end_host','store_id'];



    public function store() {
        return $this->hasOne('MetaPic\Models\Store', 'id', 'store_id');
    }

    public static function getStoreWithUrl($host,$path,$storeGroupId){
        return DeeplinkingUrls::whereRaw("? LIKE CONCAT('%',end_host)", [$host] )->whereHas('store.storeGroups', function ($innerQuery) use ($storeGroupId) {
            $innerQuery->where('id', $storeGroupId);
        })->with('store')->first();


    }
}


<?php
/**
 * Created by PhpStorm.
 * User: marcusdalgren
 * Date: 2016-11-07
 * Time: 15:42
 */

namespace MetaPic\Models;

use Eloquent;

class DeeplinkSupplier extends Eloquent {
	protected $table = "deeplink_suppliers";
}

<?php
/**
 * Created by PhpStorm.
 * User: marcusdalgren
 * Date: 2016-03-07
 * Time: 15:33
 */

namespace MetaPic\Models;

use Eloquent;
use MetaPic\Services\RevenueCalculator;
use MetaPic\Services\ShopelloService;


class TagCpcRevenue extends Eloquent {
	protected $table = "tag_cpc_revenue";
	protected $fillable = [
		"client_id",
		"user_id",
		"tag_id",
		"revenue_cpc",
		"client_revenue_cpc",
		"user_revenue_cpc",
	];

	public static function updateRevenueFromTag($tag, $cpc = null) {
		$calc = RevenueCalculator::getByUserId($tag->user_id);
		$payData = TagCpcRevenue::firstOrNew(["tag_id" => $tag->id]);
		if (!is_object($tag->user)) return $payData;

		if ($cpc == null && $payData->revenue_cpc != null) {
			$cpc = $payData->revenue_cpc;
		}
		elseif ($cpc == null && is_object($tag->product)) {
			$cpc = $tag->product->revenue_cpc;
		}
		elseif ($cpc == null && $tag->product_id !== null) {
			$shopello = ShopelloService::getShopelloApiClient($tag->user->client->feed, $tag->user->client->locale);
			try {
				$product = $shopello->getProduct($tag->product_id);
				if ($product->status == true) {
					$cpc = $product->data->revenue_cpc*100;
				}
			}
			catch (\Exception $e) {}
		}

		$payData->fill([
			"client_id" => $tag->user->client_id,
			"user_id" => $tag->user_id,
			"tag_id" => $tag->id,
			"revenue_cpc" => $cpc,
			"client_revenue_cpc" => $calc->getClientCpc($cpc),
			"user_revenue_cpc" => $calc->getCpc($cpc),
		])->save();
		return $payData;
	}
}
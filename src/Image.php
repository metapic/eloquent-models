<?php
/**
 * Created by PhpStorm.
 * User: Marcus Dalgren
 * Date: 2014-02-27
 * Time: 21:47
 */
namespace MetaPic\Models;

use Eloquent;

/**
 * An Eloquent Model: 'MetaPic\Models\Image'
 *
 * @property integer $id
 * @property string $url
 * @property string $site
 * @property integer $user_id
 * @property string $ip
 * @property integer $width
 * @property integer $height
 * @property float $ratio
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \MetaPic\Models\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\MetaPic\Models\Tag[] $tags
 */
class Image extends Eloquent {
	protected $fillable = array(
        "url",
        "site",
        "user_id",
        "width",
        "height",
        "ratio",
        "ip",
		"collage_id");
	protected $table = "images";
    protected $softDelete = true;

	public function getIdAttribute($value) {
		return ($value === null) ? null : (int)$value;
	}

	public function getHeightAttribute($value) {
		return ($value === null) ? null : (int)$value;
	}

	public function getWidthAttribute($value) {
		return ($value === null) ? null : (int)$value;
	}

	public function getRatioAttribute($value) {
		return ($value === null) ? null : (float)$value;
	}

	public function getUserIdAttribute($value) {
		return ($value == null) ? null : (int)$value;
	}

	public function user() {
		return $this->belongsTo('MetaPic\Models\User', 'user_id', 'id');
	}

	public function tags() {
		return $this->hasMany('MetaPic\Models\Tag', 'image_id', 'id');
	}

	public function clicksAndEarnings(){
		return $this->hasOne('MetaPic\Models\ImageClickView', 'image_id', 'id')
			->selectRaw('image_id, sum(clicks) as clicks, sum(earned) as earned')
			->groupBy('image_id');
	}

    public function ImageClickView(){
        return $this->hasOne('MetaPic\Models\ImageClickView', 'image_id', 'id');
    }

	public function calculateRatio() {
		$this->ratio = $this->width / $this->height;
		return $this->ratio;
	}
    public function imageViewsSum(){
        return $this->hasOne('MetaPic\Models\ImageViewsSum', 'image_id', 'id');
    }

/*
protected $appends = array("tags");
	public function getTagsAttribute() {
		return $this->tags()->get()->toArray();
	}*/
}
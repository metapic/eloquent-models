<?php
/**
 * Created by PhpStorm.
 * User: bark
 * Date: 2014-12-18
 * Time: 15:51
 */
namespace MetaPic\Models;

use Eloquent;

class UserError extends Eloquent
{
    protected $table = "user_errors";
    protected $fillable = array(
        "user_id",
        "date"
    );


    public function user() {
        return $this->belongsTo('MetaPic\Models\User', 'user_id', 'id');
    }


}


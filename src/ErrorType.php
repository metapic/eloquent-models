<?php
/**
 * Created by PhpStorm.
 * User: bark
 * Date: 2014-12-18
 * Time: 15:51
 */
namespace MetaPic\Models;

use Eloquent;

class ErrorType extends Eloquent
{
    protected $table = "error_types";
    protected $fillable = array(
        "id",
        "name",
        "description"
    );

}


<?php
/**
 * Created by PhpStorm.
 * User: Marcus Dalgren
 * Date: 2014-09-28
 * Time: 01:06
 */

namespace MetaPic\Models;


use Eloquent;

/**
 * An Eloquent Model: 'TagClickView'
 *
 * @property integer                      $id
 * @property integer                      $tag_id
 * @property integer                      $user_id
 * @property \Carbon\Carbon               $date
 * @property integer                      $clicks
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */

class TagClickView extends Eloquent {
	protected $table = "tag_click_views";
	protected $fillable = [
		"tag_id",
		"image_id",
		"user_id",
		"date",
		"clicks"
	];

	public function users()
	{
		return $this->hasMany('MetaPic\Models\User', 'client_id', 'id');
	}
    public function tag() {
        return $this->belongsTo('MetaPic\Models\Tag', 'tag_id', 'id');
    }


} 
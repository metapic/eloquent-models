<?php
/**
 * Created by PhpStorm.
 * User: bark
 * Date: 2014-12-18
 * Time: 15:51
 */
namespace MetaPic\Models;

use Eloquent;


class Favorite extends Eloquent
{
    protected $table = "favorites";
    //protected $softDelete = true;
    protected $fillable = array(
        "user_id",
        "product_id",
        "product_information");

}


<?php
/**
 * Created by PhpStorm.
 * User: Raket
 * Date: 2015-01-30
 * Time: 15:36
 */

namespace MetaPic\Models;

use Eloquent;

/**
 * An Eloquent Model: 'MetaPic\Models\ShopelloBrand'
 *
 * @property integer $id
 * @property string $locale
 * @property string $name
 * @property string $key
 * @property integer $num_products
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class ShopelloBrand extends Eloquent {
	public $incrementing = false;
	protected $fillable = array("id", "locale", "name", "key", "num_products");
	protected $table = "brands_shopello";
}
<?php
/**
 * Created by PhpStorm.
 * User: marcus
 * Date: 2014-04-09
 * Time: 09:58
 */

namespace MetaPic\Models;

use Exception;
use Guzzle\Http\Client as GuzzleClient;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Guzzle\Http\Exception\RequestException;
use Guzzle\Http\Message\Request;
use Guzzle\Http\Exception\MultiTransferException;
use Guzzle\Http\QueryString;

class Shopello {
	/**
	 * API endpoint
	 *
	 * @access private
	 */
	private $_api_endpoint = 'https://api.shopello.se/';

	/**
	 * API key
	 *
	 * @access private
	 */
	private $_api_key;

	private $client;

	/**
	 * Constructor
	 *
	 * @param string
	 */
	public function __construct($api_key = null) {
		if ($api_key !== null) {
			$this->set_api_key($api_key);
		}
	}

	/**
	 * Set API endpoint
	 *
	 * @param string
	 *
	 * @return void
	 */
	public function set_api_endpoint($api_endpoint) {
		$this->_api_endpoint = $api_endpoint;
	}

	/**
	 * Get API endpoint
	 *
	 * @return string
	 */
	public function get_api_endpoint() {
		return $this->_api_endpoint;
	}

	/**
	 * Set API key
	 *
	 * @param string
	 *
	 * @return void
	 */
	public function set_api_key($api_key) {
		$this->_api_key = $api_key;
	}

	/**
	 * Get API key
	 *
	 * @return string
	 */
	public function get_api_key() {
		return $this->_api_key;
	}

	/**
	 * Call
	 *
	 * @param string
	 * @param array
	 * @param bool
	 *
	 * @throws Exception
	 * @return array
	 */
	public function call($method, $params = array(), $post = false) {
		// Assemble the URL
		$client = new GuzzleClient($this->get_api_endpoint());
		$client->setDefaultOption('headers', ['X-API-KEY' => $this->get_api_key()]);

		$callCommand = ($post === false) ? "get" : "post";

		/* @var Request $request */
		$request = $client->$callCommand("/1/".$method . '.json');
		$responseJson = "";
		if (!$post) {
			$request->getQuery()->merge($params);
		}
		else {
			$request->addPostFields($params);
		}
		try {
			$response = $request->send();
			$responseJson = $response->json();
		} catch (ClientErrorResponseException $e) {
			$responseJson = $e->getResponse();
		}
		// Return data
		return $responseJson;
	}

	/**
	 * Products
	 *
	 * @param array|integer
	 * @param array
	 *
	 * @return array
	 */
	public function products($product_id = null, $params = array()) {
		$method = 'products';

		if (is_array($product_id)) {
			$params = $product_id;
		}
		else {
			$method .= '/' . $product_id;
		}

		return $this->call($method, $params);
	}

	/**
	 * Related products
	 *
	 * @param integer
	 *
	 * @return array
	 */
	public function related_products($product_id) {
		$method = 'related_products/' . $product_id;

		return $this->call($method, array());
	}

	/**
	 * Attributes
	 *
	 * @param array|integer
	 * @param array
	 *
	 * @return array
	 */
	public function attributes($attribute = null, $params = array()) {
		$method = 'attributes';

		if (is_array($attribute)) {
			$params = $attribute;
		}
		else {
			$method .= '/' . $attribute;
		}

		return $this->call($method, $params);
	}

	/**
	 * Stores
	 *
	 * @param array\string
	 *
	 * @return mixed
	 */
	public function stores($params) {
		$method = 'stores';
		if (!is_array($params)) {
			$method .= '/' . $params;
			$params = [];
		}

		return $this->call($method, $params);
	}

	/**
	 * Categories
	 *
	 * @param array|string
	 *
	 * @return array
	 */
	public function categories($params = null) {
		$method = 'categories';
		if (!is_array($params)) {
			$method .= '/' . $params;
			$params = [];
		}

		return $this->call($method, $params);
	}

	/**
	 * Categories
	 *
	 * @param array
	 *
	 * @return array
	 */
	public function category_parents($params = array()) {
		return $this->call('category_parents', $params);
	}

	/**
	 * Customers
	 *
	 * @param array
	 *
	 * @return array
	 */
	public function customers($params = array()) {
		return $this->call('customers', $params);
	}

	/**
	 * Batch
	 *
	 * @param array
	 *
	 * @return array
	 */
	public function batch($batch = array()) {
		return $this->call('batch', array(
			'batch' => $batch
		), true);
	}
}
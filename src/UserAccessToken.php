<?php
/**
 * Created by PhpStorm.
 * User: marcus
 * Date: 2014-03-24
 * Time: 16:06
 */

namespace MetaPic\Models;
use Eloquent;
use Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * An Eloquent Model: 'MetaPic\Models\UserAccessToken'
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $access_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \MetaPic\Models\User $user
 */
class UserAccessToken extends Eloquent {
	protected $table = "user_access_tokens";
	protected $guarded = array();

	/**
	 * @param $user
	 *
	 * @return UserAccessToken
	 */
	public static function createToken($user) {
		$accessToken = new UserAccessToken(["user_id" => $user->id, "access_token" =>


            Hash::make(
				$user->id.
				$user->username.
				$user->email.
				$user->created_at.
				$user->updated_at.
				time()
			)
		]);
		$accessToken->save();
		return $accessToken;
	}

	public static function getToken($user) {
		try {
			$token = UserAccessToken::where("user_id", "=", $user->id)->firstOrFail();
			return $token;
		}
		catch (ModelNotFoundException $e) {
			return static::createToken($user);
		}
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user() {
		return $this->belongsTo('MetaPic\Models\User', 'user_id', 'id');
	}
} 
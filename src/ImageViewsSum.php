<?php
/**
 * Created by PhpStorm.
 * User: marcus
 * Date: 2014-02-28
 * Time: 10:54
 */

namespace MetaPic\Models;

use Carbon\Carbon;
use DB;
use Eloquent;

/**
 * An Eloquent Model: 'UserClickView'
 *
 * @property integer                      $user_id
 * @property \Carbon\Carbon               $date
 * @property integer                      $link_clicks
 * @property integer                      $tag_clicks
 * @property integer                      $views
 * @property integer                      $affiliate
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class ImageViewsSum extends Eloquent {
	protected $table = "image_views_sum";
	protected $fillable = array(
		"image_id",
        "views"
	);
    public function image(){
        return $this->belongsTo('MetaPic\Models\Image', 'id','image_id');
    }


}
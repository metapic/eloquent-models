<?php
/**
 * Created by PhpStorm.
 * User: marcus
 * Date: 2014-04-10
 * Time: 16:22
 */

namespace MetaPic\Models;

use Eloquent;

/**
 * An Eloquent Model: 'MetaPic\Models\ShopelloStore'
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $alias
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class ShopelloStore extends Eloquent {
	public $incrementing = false;
	protected $fillable = array("id", "name", "url", "alias","locale","site_url", "revenue_cpc", "feed", "has_deeplink");
	protected $table = "stores_shopello";
}

<?php
/**
 * Created by PhpStorm.
 * User: Raket
 * Date: 2014-05-26
 * Time: 12:19
 */

namespace MetaPic\Models;

use Eloquent;

class RetailerRequest extends Eloquent  {
	protected $fillable = array("name", "email", "request", "client_id");
	protected $table = "retailer_requests";
} 
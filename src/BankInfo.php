<?php
/**
 * Created by PhpStorm.
 * User: bark
 * Date: 2014-12-18
 * Time: 15:51
 */
namespace MetaPic\Models;

use Eloquent;

class BankInfo extends Eloquent
{
    protected $table = "bank_info";
    protected $hidden=["updated_at","created_at","deleted_at"];

    protected $softDelete = true;

    public function user() {
        return $this->belongsTo('MetaPic\Models\User', 'user_id', 'id');
    }
}


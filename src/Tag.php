<?php
/**
 * Created by PhpStorm.
 * User: marcus
 * Date: 2014-02-28
 * Time: 10:54
 */

namespace MetaPic\Models;

use Eloquent;
use Config;

/**
 * An Eloquent Model: 'MetaPic\Models\Tag'
 *
 * @property integer                      $id
 * @property float                        $x
 * @property float                        $y
 * @property float                        $width
 * @property float                        $height
 * @property string                       $link
 * @property integer                      $image_id
 * @property string                       $text
 * @property string                       $type
 * @property string                       $meta_info
 * @property integer                      $user_id
 * @property integer                      $product_id
 * @property boolean                      $out_of_stock
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \MetaPic\Models\User $user
 * @property-read \MetaPic\Models\Image   $image
 * @method array|string deleteTag(\int $tagId, array $tagData)
 */
class Tag extends Eloquent {
	protected $table = "tags";
	protected $fillable = array(
		"x",
		"y",
		"width",
		"height",
		"link",
		"image_id",
		"text",
		"type",
        "ip",
		"meta_info",
		"user_id",
		"product_id",
        "tier_pricing_id",
        "created_as",
        "affiliate_link",
		"store_id",
		"store"
	);
	protected $appends = array('mtpc_url');

	protected $softDelete = true;

	public function getIdAttribute($value) {
		return ($value === null) ? null : (int)$value;
	}

	public function getXAttribute($value) {
		return ($value === null) ? null : (float)$value;
	}

	public function getYAttribute($value) {
		return ($value === null) ? null : (float)$value;
	}

	public function getWidthAttribute($value) {
		return ($value === null) ? null : (float)$value;
	}

	public function getHeightAttribute($value) {
		return ($value === null) ? null : (float)$value;
	}

	public function getImageIdAttribute($value) {
		return ($value === null) ? null : (int)$value;
	}

	public function getUserIdAttribute($value) {
		return ($value === null) ? null : (int)$value;
	}

	public function getProductIdAttribute($value) {
		return ($value === null) ? null : (int)$value;
	}

	public function getMtpcUrlAttribute($value) {
		return Config::get("metapic.click_link").$this->attributes['id'];
	}


	public function clicksAndEarnings(){
		return $this->hasOne('MetaPic\Models\TagClickView', 'tag_id', 'id')
			->selectRaw('tag_id, sum(clicks) as clicks, sum(earned) as earned')
			->groupBy('tag_id');
	}


	public function user() {
		return $this->belongsTo('MetaPic\Models\User', 'user_id', 'id');
	}

	public function image() {
		return $this->belongsTo('MetaPic\Models\Image', 'image_id', 'id');
	}

    public function linkClicks() {
        return $this->hasMany('MetaPic\Models\LinkClicks', 'tag_id', 'id');
    }
    public function tagClickView(){
        return $this->hasOne('MetaPic\Models\TagClickView', 'tag_id', 'id');
    }
 

	public function product() {
		return $this->belongsTo('MetaPic\Models\Product', 'product_id', 'id');
	}

	public function cpcRevenue() {
		return $this->hasOne('MetaPic\Models\TagCpcRevenue', 'tag_id', 'id');
	}
}
<?php
/**
 * Created by PhpStorm.
 * User: bark
 * Date: 2014-12-18
 * Time: 15:51
 */
namespace MetaPic\Models;

use Eloquent;

class Store extends Eloquent
{
    protected $table = "stores";
    protected $fillable = ['feed_id','feed_id','feed_name','feed_provider','language_iso_code','currency_iso_code','locale','revenue_cpc','url','feed'];

    protected $softDelete = false;

    public function urls() {
        return $this->hasMany('MetaPic\Models\DeeplinkingUrls', 'store_id', 'id');
    }
    public function StoreGroups() {
        return $this->belongsToMany('MetaPic\Models\StoreGroup', 'store_groups_to_stores');
    }
}


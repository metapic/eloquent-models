<?php
/**
 * Created by PhpStorm.
 * User: bark
 * Date: 2014-12-18
 * Time: 15:51
 */
namespace MetaPic\Models;

use Eloquent;

class StoreGroup extends Eloquent
{
    protected $table = "store_groups";

    protected $softDelete = false;

    public function stores() {
        return $this->belongsToMany('MetaPic\Models\Store', 'store_groups_to_stores','');
    }
}


<?php
/**
 * Created by PhpStorm.
 * User: marcus
 * Date: 2014-04-24
 * Time: 11:50
 */

namespace MetaPic\Models;

use Eloquent;

/**
 * An Eloquent Model: 'MetaPic\Models\ClientUserConfig'
 *
 * @property integer $id
 * @property integer $client_id
 * @property bool $background_off
 * @property bool $show_all_tags
 * @property bool $picture_menu_visible
 * @property bool $strict_mode
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \MetaPic\Models\Client $client
 */
class ClientUserConfig extends Eloquent {
	protected $fillable = array("client_id", "background_off", "show_all_tags", "picture_menu_visible", "strict_mode");
	protected $table = "client_user_config";
	protected $hidden = array('created_at', 'updated_at');

	public function getIdAttribute($value) {
		return (int)$value;
	}

	public function getClientIdAttribute($value) {
		return (int)$value;
	}

	public function getBackgroundOffAttribute($value) {
		return (bool)$value;
	}

	public function getShowAllTagsAttribute($value) {
		return (bool)$value;
	}

	public function getPictureMenuVisibleAttribute($value) {
		return (bool)$value;
	}

	public function getStrictModeAttribute($value) {
		return (bool)$value;
	}

	public function client() {
		return $this->belongsTo('MetaPic\Models\Client', 'client_id', 'id');
	}
}
<?php
/**
 * Created by PhpStorm.
 * User: marcus
 * Date: 2014-04-10
 * Time: 16:22
 */

namespace MetaPic\Models;

use Eloquent;

/**
 * An Eloquent Model: 'MetaPic\Models\PrisjaktProduct'
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $alias
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class ProductsPrisjakt extends Eloquent {
	protected $fillable = array("id", "name", "category_id", "image_url","out_of_stock_at");
	protected $table = "products_prisjakt";
} 
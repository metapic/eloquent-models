<?php
/**
 * Created by PhpStorm.
 * User: bark
 * Date: 2014-12-18
 * Time: 15:51
 */
namespace MetaPic\Models;

use Eloquent;

class IframeTokens extends Eloquent
{
    protected $table = "iframe_tokens";
    protected $fillable = array(
        "user_id",
        "random_token"
    );
    protected $softDelete = true;

    public function user() {
        return $this->belongsTo('MetaPic\Models\User', 'user_id', 'id');
    }
}


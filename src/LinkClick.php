<?php
namespace MetaPic\Models;
use Eloquent;
use DB;

/**
 * An Eloquent Model: 'LinkClick'
 *
 */
class LinkClick extends Eloquent {
	protected $table = "link_clicks";
	protected $fillable = array(
		"tag_id",
		"ip",
		"redirected_from",
		"created_at",
		"country_code",
		"too_many_clicks",
		"updated_at"
	);

    public function tag() {
        return $this->belongsTo('MetaPic\Models\Tag', 'tag_id', 'id');
    }




    public static function getPrisJaktStats($from, $to,$place) {
        $query = "SELECT * FROM
		(SELECT email,sum(page_views) as page_views,sum(tag_views) as tag_views,sum(image_views) as image_views,sum(tag_clicks) as tag_clicks,sum(link_clicks) as link_clicks,user_id
			FROM user_click_views,users
			WHERE users.id=user_click_views.user_id AND users.client_id REGEXP '{$place}' AND date>='{$from}' AND date<='{$to}' GROUP BY user_id) as user_click_views
		LEFT JOIN
		(SELECT sum(sum_sek) as sum_sek,user_id as UtUserId
			FROM payments
			WHERE payment_date>'{$from}' AND payment_date<'{$to}' GROUP BY user_id) as payments
		ON payments.UtUserId=user_click_views.user_id";

        $results = DB::select($query);
        return $results;
    }




        public static function getCheatStatsicsPerUser($from, $to,$place) {

            $query =  "SELECT email,user_errors.user_id,IFNULL(views,0) as views,IFNULL(clicks,0) as clicks,clicks_one_ip_one_tag,out_of_stock,clicks_one_ip,own_tag,wrong_country,not_payable FROM users,`user_errors`
              LEFT JOIN
                (SELECT user_click_views.user_id,SUM(page_views) as views,SUM(link_clicks+tag_clicks) as clicks
                 FROM user_click_views
                 WHERE  date>='{$from}' AND date<='{$to}'
              GROUP BY user_click_views.user_id) as clicks
            ON clicks.user_id=user_errors.user_id
            WHERE user_errors.user_id=users.id AND users.client_id LIKE '{$place}' AND date>='{$from}' AND date<='{$to}'
            group by user_errors.user_id";


            $results = DB::select($query);
        return $results;
    }



    public static function getPayment($month)
    {
        $query = "SELECT user_click_views.user_id,sum(link_clicks)+sum(tag_clicks)-IFNULL(outPayment.money,0) as pay FROM `user_click_views`
                LEFT JOIN
                    (SELECT user_id,sum(sum_sek) as money
                        FROM payments
                        WHERE payment_date<'".$month."'
                        GROUP BY user_id
                    ) as  outPayment
                ON outPayment.user_id=user_click_views.user_id
                LEFT JOIN users ON user_click_views.user_id=users.id
                LEFT JOIN clients ON users.client_id=clients.id
                WHERE clients.own_paymentsystem = 0 AND user_click_views.date<'".$month."'
                GROUP BY user_click_views.user_id
                HAVING pay>200";

        $results = DB::select($query);

        return $results;
    }


}

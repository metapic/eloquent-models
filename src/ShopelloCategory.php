<?php
/**
 * Created by PhpStorm.
 * User: Raket
 * Date: 2015-01-30
 * Time: 15:36
 */

namespace MetaPic\Models;

use Eloquent;

/**
 * An Eloquent Model: 'MetaPic\Models\ShopelloCategory'
 *
 * @property integer $id
 * @property string $locale
 * @property string $name
 * @property string $key
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class ShopelloCategory extends Eloquent {
	public $incrementing = false;
	protected $fillable = array("id", "locale", "name", "key");
	protected $table = "categories_shopello";
}
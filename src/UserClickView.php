<?php
/**
 * Created by PhpStorm.
 * User: marcus
 * Date: 2014-02-28
 * Time: 10:54
 */

namespace MetaPic\Models;

use Carbon\Carbon;
use DB;
use Eloquent;
use Log;

/**
 * An Eloquent Model: 'UserClickView'
 *
 * @property integer                      $user_id
 * @property \Carbon\Carbon               $date
 * @property integer                      $link_clicks
 * @property integer                      $tag_clicks
 * @property integer                      $views
 * @property integer                      $affiliate
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class UserClickView extends Eloquent {
	protected $table = "user_click_views";
	protected $fillable = array(
		"user_id",
		"date",
		"link_clicks",
		"tag_clicks",
		"affiliate",
		"created_at",
		"updated_at",
		"earned"
	);

	public static function getAllClicksForClient(Client $client, $userId = null, $from = "2012-01-01", $to = "2112-01-01") {
		$userString = is_numeric($userId) ? "AND users.id = {$userId}" :"";
		$from = Carbon::createFromTimestamp(strtotime($from))->toDateTimeString();
		$to = Carbon::createFromTimestamp(strtotime($to))->addDay()->toDateTimeString();
		$dateString = "link_clicks.created_at >= '{$from}' AND link_clicks.created_at < '{$to}'";

		$newQuery = "SELECT * FROM (SELECT tags.id AS tag_id, users.id AS user_id, users.email
			, SUM(if(tags.image_id IS NULL, 1, 0)) AS link_clicks
			, SUM(if(tags.image_id IS NOT NULL, 1, 0)) AS tag_clicks
			, COUNT(link_clicks.id) AS total_clicks
			, SUM(tag_cpc_revenue.user_revenue_cpc) AS user_revenue,
			SUM(tag_cpc_revenue.client_revenue_cpc) AS client_revenue
			FROM users
			JOIN tags ON users.id = tags.user_id
			JOIN link_clicks ON tags.id = link_clicks.tag_id
			JOIN tag_cpc_revenue ON tags.id = tag_cpc_revenue.tag_id
			WHERE
			users.client_id = {$client->id}
			AND link_clicks.too_many_clicks = 0
			AND {$dateString}
			{$userString}
			GROUP BY users.id) AS user_click_views
			LEFT JOIN
			(SELECT sum(sum_sek) as sum_sek,user_id as ut_user_id
				FROM payments
				WHERE payment_date >= '{$from}' AND payment_date < '{$to}' GROUP BY user_id) as payments
			ON payments.ut_user_id = user_click_views.user_id";
		$results = DB::select(DB::raw($newQuery));
		$returnArray = array_map(function($result) {
			return [
				"email" => $result->email,
				"tag_clicks" => (int)$result->tag_clicks,
				"link_clicks" => (int)$result->link_clicks,
				"total_clicks" => (int)$result->total_clicks,
				"user_revenue" => (int)$result->user_revenue,
				"client_revenue" => (int)$result->client_revenue,
				"affiliate" => 0,
				"user_id" => $result->user_id,
				"sum_sek" => $result->sum_sek,
				"ut_user_id" => $result->ut_user_id,
			];
		}, $results);

		return $returnArray;
	}

	public static function emptyUserClick($userId) {
		return 	[
			"email" => "",
			"tag_clicks" => 0,
			"link_clicks" => 0,
			"affiliate" => 0,
			"total_clicks" => 0,
			"user_cpc" => 0,
			"client_cpc" => 0,
			"user_id" => (int)$userId,
			"sum_sek" => 0,
			"ut_user_id" => null
		];
	}

	public static function getAllClicksForClientByDate($clientId, $userId = null, $from = "2012-01-01", $to = "2112-01-01") {
		$userString = "";
		$queryVars = [
			"client_id" => $clientId
		];

		if (is_numeric($userId)) {
			$userString = "AND users.id = :user_id";
			$queryVars["user_id"] = $userId;
		}

		$dayQuery = "SELECT `date`, link_clicks, tag_clicks, user_id, email FROM user_click_views
			LEFT JOIN users ON user_click_views.user_id = users.id
			WHERE  `date` >= :date_from AND `date` <= :date_to AND users.client_id = :client_id $userString ORDER BY `date` DESC";

		$monthQuery = "SELECT SUM(link_clicks + tag_clicks) AS total_clicks, user_id, email FROM user_click_views
			LEFT JOIN users ON user_click_views.user_id = users.id
			WHERE  YEAR(`date`) = YEAR(NOW()) AND MONTH(`date`) = MONTH(NOW()) AND users.client_id = :client_id $userString
			GROUP BY users.id";

		$totalQuery = "SELECT SUM(link_clicks + tag_clicks) AS total_clicks, user_id, email FROM user_click_views
			LEFT JOIN users ON user_click_views.user_id = users.id
			WHERE users.client_id = :client_id $userString
			GROUP BY users.id";
		$dayResults = array_reduce(DB::select($dayQuery, array_merge($queryVars, ["date_from" => $from, "date_to" => $to])),
			function($carry, $item) {
				$item = (array)$item;
				$item["tag_clicks"] = (int)$item["tag_clicks"];
				$item["link_clicks"] = (int)$item["link_clicks"];
				if (isset($carry[$item["email"]]["day"]))
					$carry[$item["email"]]["day"][] = $item;
				else
					$carry[$item["email"]]["day"] = [$item];
				return $carry;
			},
		[]);
		$queries = DB::getQueryLog();
		$last_query = end($queries);
		\Log::info($last_query);

		$monthResults = array_reduce(DB::select($monthQuery, $queryVars), function($carry, $item) {
			$carry[$item->email]["month"] = (int)$item->total_clicks;
			return $carry;
		}, []);

		$totalResults = array_reduce(DB::select($totalQuery, $queryVars), function($carry, $item) {
			$carry[$item->email]["total"] = (int)$item->total_clicks;
			return $carry;
		}, []);

		return array_merge_recursive($dayResults, $monthResults, $totalResults);
	}
}
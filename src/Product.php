<?php

namespace MetaPic\Models;
use Eloquent;

class Product extends Eloquent {
	public $incrementing = false;
	protected $fillable = array("id", "name", "store_id","feed","brand_id","category_id", "image_url","original_url", "out_of_stock_at");
	protected $table = "products";

	public function tags() {
		return $this->hasMany('MetaPic\Models\Tag', 'product_id', 'id');
	}
}
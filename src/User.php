<?php
/**
 * Created by PhpStorm.
 * User: Marcus Dalgren
 * Date: 2014-02-27
 * Time: 21:29
 */

namespace MetaPic\Models;

use Eloquent;
use Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\SoftDeletingTrait;


/**
 * An Eloquent Model: 'MetaPic\Models\User'
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $surname
 * @property string $country
 * @property string $city
 * @property string $address
 * @property string $postcode
 * @property string $SSN
 * @property string $tier_pricing_type
 * @property \Carbon\Carbon $verified_at
 * @property \Carbon\Carbon $last_signed_in_at
 * @property \Carbon\Carbon $reset_sent_at
 * @property \Carbon\Carbon $suspended_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $client_id
 * @property integer $channel_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\MetaPic\Models\Image[] $images
 * @property-read \Illuminate\Database\Eloquent\Collection|\MetaPic\Models\Tag[] $tags
 * @property-read \MetaPic\Models\Client $client
 * @property-read \MetaPic\Models\RevenueTier $revenueTier
 * @property-read \MetaPic\Models\UserConfig $userConfig
 */
class User extends Eloquent
{
	protected $fillable = array("username", "email", "tier_pricing_type");
	protected $hidden = array('user_client_id', 'user_secret_key', 'password', 'client_id', 'verified_at', 'last_signed_in_at', 'reset_sent_at', 'suspended_at', 'deleted_at', 'SSN', 'channel_id','');
	protected $table = "users";
	use SoftDeletingTrait;

	public static function getAuthData($userClientId)
	{
		$user = User::where("user_client_id", "=", $userClientId)->firstOrFail();
		return ["client_id" => $userClientId, "secret_key" => $user->user_secret_key];
	}

	public function getIdAttribute($value)
	{
		return ($value == null) ? null : (int)$value;
	}

	public function getClientIdAttribute($value)
	{
		return ($value == null) ? null : (int)$value;
	}

	public function images()
	{
		return $this->hasMany('MetaPic\Models\Image', 'user_id', 'id');
	}

	public function tags()
	{
		return $this->hasMany('MetaPic\Models\Tag', 'user_id', 'id');
	}

	public function tokens()
	{
		return $this->hasMany('MetaPic\Models\UserAccessToken', 'user_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function client()
	{
		return $this->belongsTo('MetaPic\Models\Client', 'client_id', 'id');
	}

	public function revenueTier()
	{
		return $this->belongsTo('MetaPic\Models\RevenueTier', 'revenue_tier_id', 'id');
	}

	public function bankInfo()
	{
		return $this->hasMany('MetaPic\Models\BankInfo',  'user_id','id');
	}

	/**
	 * @return UserAccessToken
	 */
	public function getAccessToken()
	{
		return UserAccessToken::getToken($this);
	}


	public function getChannelName()
    {
        return preg_replace("/[^A-Za-z0-9 ]/", '', $this->email) . $this->id;
    }

	/**
	 * @param $token
	 * @return User|mixed|static
	 * @throws ModelNotFoundException
	 */
	public static function getByAccessToken($token) {
		$token = UserAccessToken::where("access_token","=",$token)->firstOrFail();
		if ($token->user == null) throw new ModelNotFoundException("User not found");
		return $token->user;
	}



	/**
	 * @param User $user
	 * @param bool
	 * @return User
	 */
	public static function getUserData(User $user)
	{
		$userData = $user->toArray();
		$userData["client"] = $user->client->toArray();
		if (is_object($user->revenueTier))
			$userData["revenue_tier"] = $user->revenueTier->toArray();

		$user->client->storeGroup->toArray();
		return $user;
	}
}

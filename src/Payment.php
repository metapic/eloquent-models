<?php
/**
 * Created by PhpStorm.
 * User: bark
 * Date: 2014-12-18
 * Time: 15:51
 */
namespace MetaPic\Models;

use Eloquent;

class Payment extends Eloquent
{
    protected $fillable = array(
        "user_id",
        "sum_sek",
        "comment",
        "payment_date"
    );
    protected $table = "payments";

    protected $softDelete = true;

    public function user() {
        return $this->belongsTo('MetaPic\Models\User', 'user_id', 'id');
    }
}


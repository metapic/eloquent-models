<?php
/**
 * Created by PhpStorm.
 * User: Raket
 * Date: 2014-05-26
 * Time: 12:19
 */

namespace MetaPic\Models;

use Eloquent;

class SearchQuery extends Eloquent  {
	protected $fillable = array("user_id", "query", "category", "hit", "query_history", "lead_to_save", "hits_history", "category_history", "tag_id");
	protected $table = "search_history";
	public $incrementing = false;
} 
<?php

namespace MetaPic\Models;
use Eloquent;

class ShopelloProduct extends Eloquent {
	public $incrementing = false;
	protected $fillable = array("id", "name", "category_id", "image_url", "link", "out_of_stock_at");
	protected $table = "products_shopello";
}